package fr.fondespierre.beweb.java.ee.spring.apiclient.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apiclient.entities.Customer;
import fr.fondespierre.beweb.java.ee.spring.apiclient.exceptions.MysqlException;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
	
	@Query(
			value = "SELECT * FROM devis.customer where devis.customer.last_name = ?1",
			nativeQuery = true
			)
	List<Customer> findByLastName (String lastName) throws MysqlException;

}
