package fr.fondespierre.beweb.java.ee.spring.apiclient.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apiclient.entities.Customer;
import fr.fondespierre.beweb.java.ee.spring.apiclient.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apiclient.services.CustomerService;

@RestController
@RequestMapping(value = "rest/customers")
@CrossOrigin(origins = "*")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;

	@GetMapping
	public ResponseEntity<?> listCustomers() {
		List<Customer> customers = null;
		try {
			customers = this.customerService.list();
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getCustomer(@PathVariable Long id) {
		Customer customer = null;
		try {
			customer = this.customerService.get(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/search/{lastName}")
	public ResponseEntity<?> searchCustomersByLastName(@PathVariable("lastName") String lastName) {
		List<Customer> customers = null;
		try {
			customers = this.customerService.searchByLastName(lastName);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}
	
}
