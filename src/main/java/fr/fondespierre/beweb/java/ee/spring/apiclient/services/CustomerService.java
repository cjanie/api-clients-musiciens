package fr.fondespierre.beweb.java.ee.spring.apiclient.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apiclient.entities.Customer;
import fr.fondespierre.beweb.java.ee.spring.apiclient.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apiclient.repositories.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public List<Customer> list() throws MysqlException {
		List<Customer> customers = new ArrayList<>();
		this.customerRepository.findAll().forEach(customer -> {
			customers.add(customer);
		});
		return customers;
	}
	
	public Customer get(Long id) throws MysqlException {
		return this.customerRepository.findById(id).get();
	}
	
	public List<Customer> searchByLastName(String lastName) throws MysqlException {
		return this.customerRepository.findByLastName(lastName);
	}

}
