package fr.fondespierre.beweb.java.ee.spring.apiclient.exceptions;

public class MysqlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MysqlException() {
		super("Mysql server error");
	}
}
